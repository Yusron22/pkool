import 'dart:convert';

class ActivityLog {
  String activityNo;
  String activityTime;
  String activityStatus;
  String activityNotes;

  ActivityLog({ this.activityNo, this.activityTime,this.activityStatus,this.activityNotes});

  factory ActivityLog.fromJson(Map<String, dynamic> map) {
    return ActivityLog(
        activityNo: map["act_no"], activityTime: map["act_time"], activityStatus:map["act_status"], activityNotes: map["act_notes"]);
  }

  Map<String, dynamic> toJson() {
    return {"act_no": activityNo, "act_time": activityTime,"act_status": activityStatus,"act_notes": activityNotes};
  }

  @override
  String toString() {
    return 'ActivityLog{"act_no": $activityNo, "act_time": $activityTime,"act_status": $activityStatus,"act_notes": $activityNotes}';
  }

}

List<ActivityLog> activityLogFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<ActivityLog>.from(data.map((item) => ActivityLog.fromJson(item)));
}

String activityLogToJson(ActivityLog data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}