import 'dart:convert';

class Booking {
  int bookingNo;
  String bookingBy;
  String bookingTimeFrom;
  String bookingTimeTo;
  String bookingTo;
  String bookingReason;
  int bookingStatus;
  String bookingApprovalBy;
  String notes;


  
  Booking({ this.bookingNo, this.bookingBy, 
    this.bookingTimeFrom,this.bookingTimeTo,this.bookingTo,this.bookingReason,this.bookingStatus,
    this.bookingApprovalBy,this.notes});

  factory Booking.fromJson(Map<String, dynamic> map) {
    return Booking(
        bookingNo: map["booking_no"], bookingBy: map["booking_by"], bookingTimeFrom: map["booking_time_from"], 
        bookingTimeTo: map["booking_time_to"],bookingTo:map["booking_to"],
        bookingReason: map["booking_reason"],bookingStatus:map["booking_status"],
        bookingApprovalBy: map["booking_approvalBy"],notes:map["notes"]
        );
  }

  Map<String, dynamic> toJson() {
    return {"booking_no": bookingNo,"booking_by":bookingBy, 
        "booking_time_from": bookingTimeFrom, "booking_time_to": bookingTimeTo, 
        "booking_to": bookingTo,"booking_reason":bookingReason,
        "booking_status":bookingStatus,"booking_approvalBy": bookingApprovalBy,
        "notes":notes};
  }

  Map<String, dynamic> toJson01() {
    return {"booking_no": bookingNo,"booking_by":bookingBy};
  }

  Map<String, dynamic> toJson02() {
    return {"booking_no": bookingNo,"booking_by":bookingBy,"booking_status":bookingStatus};
  }

  Map<String, dynamic> toJson03() {
    return {"booking_no": bookingNo};
  }


  @override
  String toString() {
    return 'Booking{booking_no: $bookingNo,booking_by:$bookingBy,booking_time_from: $bookingTimeFrom, booking_time_to: $bookingTimeTo, booking_to: $bookingTo,booking_reason:$bookingReason,booking_status:$bookingStatus,booking_approvalBy:$bookingApprovalBy,notes:$notes}';
  }

}

List<Booking> bookingFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Booking>.from(data.map((item) => Booking.fromJson(item)));
}

String bookingToJson(Booking data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}

String cancelBookingToJson(Booking data){
  final jsonData = data.toJson01();
  return json.encode(jsonData);
}

String approvalBookingToJson(Booking data){
  final jsonData = data.toJson02();
  return json.encode(jsonData);
}

String releaaseBookingToJson(Booking data)
{
  final jsonData = data.toJson03();
  return json.encode(jsonData);  
}