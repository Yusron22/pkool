import 'dart:convert';

class Booking {
  String bookingDate;
  String timeFrom;
  String timeTo;
  String bookingBy;
  String bookingNo;
  String bookingTo;
  String bookingReason;
  String notes;

  Booking({ this.bookingDate, this.timeFrom, this.timeTo,this.bookingBy,this.bookingNo,this.bookingTo,this.bookingReason,this.notes});

  factory Booking.fromJson(Map<String, dynamic> map) {
    return Booking(
        bookingDate: map["booking_date"], timeFrom: map["time_from"], timeTo: map["time_to"], bookingBy: map["booking_by"],bookingNo:map["booking_no"],bookingTo:map["booking_to"],bookingReason:map["booking_reason"],notes:map["notes"]);
  }

  Map<String, dynamic> toJson() {
    return {"booking_date": bookingDate, "time_from": timeFrom, "time_to": timeTo, "booking_by": bookingBy,"booking_no":bookingNo,"booking_to": bookingTo,"booking_reason":bookingReason,"notes":notes};
  }

  @override
  String toString() {
    return 'Booking{"booking_date": $bookingDate, "time_from": $timeFrom, "time_to": $timeTo, "booking_by": $bookingBy,"booking_no":$bookingNo,"booking_to": $bookingTo,"booking_reason":$bookingReason,"notes":$notes}';
  }

}

List<Booking> bookingFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Booking>.from(data.map((item) => Booking.fromJson(item)));
}

String bookingToJson(Booking data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}