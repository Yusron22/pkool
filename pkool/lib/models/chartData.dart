import 'dart:convert';

class ChartData {
  String date;
  int value;

  ChartData({ this.date, this.value});

  factory ChartData.fromJson(Map<String, dynamic> map) {
    return ChartData(
        date: map["date"], value: map["value"]);
  }

  Map<String, dynamic> toJson() {
    return {"date": date, "value": value};
  }

  @override
  String toString() {
    return 'ChartData{"date": $date, "value": $value}';
  }

}

List<ChartData> chartDataFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<ChartData>.from(data.map((item) => ChartData.fromJson(item)));
}

String chartDataLogToJson(ChartData data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}