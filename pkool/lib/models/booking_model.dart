import 'dart:convert';

class Booking {
  String username;
  String datefrom;
  String dateto;
  String tujuan;
  String alasan;

  
  Booking({ this.username, this.datefrom, this.dateto,this.tujuan,this.alasan});

  factory Booking.fromJson(Map<String, dynamic> map) {
    return Booking(
        username: map["bookingby"], datefrom: map["booking_time_from"], dateto: map["booking_time_to"], tujuan: map["booking_to"],alasan:map["booking_reason"]);
  }

  Map<String, dynamic> toJson() {
    return {"bookingby": username, "booking_time_from": datefrom, "booking_time_to": dateto, "booking_to": tujuan,"booking_reason":alasan};
  }

  @override
  String toString() {
    return 'Booking{bookingby: $username, booking_time_from: $datefrom, booking_time_to: $dateto, booking_to: $tujuan,booking_reason:$alasan}';
  }

}

List<Booking> bookingFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Booking>.from(data.map((item) => Booking.fromJson(item)));
}

String bookingToJson(Booking data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}

