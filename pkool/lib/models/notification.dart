import 'dart:convert';

class Notification {
  int notifNo;
  String notifMsg;

  
  Notification({ this.notifNo, this.notifMsg});

  factory Notification.fromJson(Map<String, dynamic> map) {
    return Notification(
        notifNo: map["notif_no"], notifMsg: map["notif_msg"]);
  }

  Map<String, dynamic> toJson() {
    return {"notif_no":notifNo, "notif_msg": notifNo};
  }

  
  @override
  String toString() {
    return 'Notification{notif_no: $notifNo, notif_msg: $notifMsg}';
  }

}

Map<String, dynamic> toJson01(String bookingno,String username) {
    return {"notif_no":bookingno, "notif_user": username};
}

List<Notification> notifFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Notification>.from(data.map((item) => Notification.fromJson(item)));
}


String notifToJson(Notification data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}

String notifToJson01(String bookingno,String username)
{
  final jsonData =toJson01(bookingno,username);
  return json.encode(jsonData);
}
