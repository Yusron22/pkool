import 'dart:convert';

class BookingStatus {
  String bookingNo;
  String notes;

  
  BookingStatus({ this.bookingNo, this.notes});

  factory BookingStatus.fromJson(Map<String, dynamic> map) {
    return BookingStatus(
        bookingNo: map["booking_no"], notes: map["notes"]);
  }

  Map<String, dynamic> toJson() {
    return {"booking_no": bookingNo, "notes": notes};
  }

  @override
  String toString() {
    return 'BookingStatus{booking_no: $bookingNo, notes: $notes}';
  }

} 

BookingStatus bookingStatusFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return BookingStatus.fromJson(data);
}

String bookingStatusToJson(BookingStatus data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
