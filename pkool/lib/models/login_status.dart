import 'dart:convert';

class LoginStatus {
  String status;
  int roleid;

  LoginStatus({ this.status, this.roleid});

  factory LoginStatus.fromJson(Map<String, dynamic> map) {
    return LoginStatus(
        status: map["status"], roleid: map["roleid"]);
  }

  Map<String, dynamic> toJson() {
    return {"status": status, "roleid": roleid};
  }

  @override
  String toString() {
    return 'LoginStatus{"status": $status, "roleid": $roleid}';
  }

}

LoginStatus loginStatusFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return LoginStatus.fromJson(data[0]);
  
}

String logoutFromJson(String jsonData)
{
  final data = json.decode(jsonData);
  return data[0]["status"];
}

String loginStatusToJson(LoginStatus data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}