import 'dart:convert';

class Contact {
  String fullname;
  String email;

  Contact({ this.fullname, this.email});

  factory Contact.fromJson(Map<String, dynamic> map) {
    return Contact(
        fullname: map["full_name"], email: map["email"]);
  }

  Map<String, dynamic> toJson() {
    return {"full_name": fullname, "email": email};
  }

  @override
  String toString() {
    return 'Contact{"full_name": $fullname, "email": $email}';
  }

}

List<Contact> contactFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Contact>.from(data.map((item) => Contact.fromJson(item)));
}

String contactToJson(Contact data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}