import 'dart:convert';

class DashboardData {
  int totalBooking;
  int totalApproved;
  int totalCancelled;

  DashboardData({ this.totalBooking, this.totalApproved,this.totalCancelled});

  factory DashboardData.fromJson(Map<String, dynamic> map) {
    return DashboardData(
        totalBooking: map["total_booking"], totalApproved: map["total_booking_approved"], totalCancelled:map["total_booking_cancelled"]);
  }

  Map<String, dynamic> toJson() {
    return {"total_booking": totalBooking, "total_booking_approved": totalApproved,"total_booking_cancelled": totalCancelled};
  }

  @override
  String toString() {
    return 'DashboardData{"total_booking": $totalBooking, "total_booking_approved": $totalApproved,"total_booking_cancelled": $totalCancelled}';
  }

}

List<DashboardData> dashboardDataFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<DashboardData>.from(data.map((item) => DashboardData.fromJson(item)));
}

String dashboardDataLogToJson(DashboardData data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}