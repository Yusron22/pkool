import 'package:pkool/models/activity_log.dart';
import 'package:pkool/models/booking_status.dart';
import 'package:pkool/models/login_status.dart';

import 'models/contact.dart';
import 'profile_model.dart';
import 'package:http/http.dart' show Client;
import 'package:global_configuration/global_configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'models/booking_model.dart' as a;
import 'models/booking.dart' as b;
import 'models/booking_daily_calendar.dart' as c;
import 'models/booking_status.dart';
import 'models/dashboard.dart';
import 'models/chartData.dart';
import 'models/notification.dart';
import 'models/login_status.dart';

class ApiService {

  String _apiURL="https://appws.lps.go.id/pkool";
  //String _apiURL="http://192.168.100.6:3000";
  String apiUrl = GlobalConfiguration().getString("api"); // prints value1
  Client client = Client();


  Future<List<Profile>> getProfiles() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString("username");
    final response = await client.get("$apiUrl/api/users/"+username);
    if (response.statusCode == 200) {
      return profileFromJson(response.body);
    } else {
      return null;
    }
  }


  Future<List<b.Booking>> getBooking() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString("username");
    final response = await client.get("$apiUrl/api/booking/"+username);
    if (response.statusCode == 200) {
      return b.bookingFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<c.Booking>> getAllBooking() async {
    final response = await client.get("$apiUrl/api/booking_calendar");
    if (response.statusCode == 200) {
      return c.bookingFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<Contact>> getContactHelpdesk() async {
    final response = await client.get("$apiUrl/api/contact_helpdesk");
    if (response.statusCode == 200) {
      return contactFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<c.Booking>> getBookingCalendar() async {
    final response = await client.get("$apiUrl/api/booking_calendar");
    if (response.statusCode == 200) {
      return c.bookingFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<ActivityLog>> getActivityLog(int bookingNo) async {
    final response = await client.get("$apiUrl/api/activity_log/" + bookingNo.toString());
    if (response.statusCode == 200) {
      return activityLogFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<DashboardData>> getDashboardData() async {
    final response = await client.get("$apiUrl/api/dashboard");
    if (response.statusCode == 200) {
      return dashboardDataFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<ChartData>> getChartData() async {
    final response = await client.get("$apiUrl/api/chartdata");
    if (response.statusCode == 200) {
      return chartDataFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<List<Notification>> getNotification() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString("username");
    final response = await client.get("$_apiURL/api/notif/"+username);
    if (response.statusCode == 200) {
      return notifFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<bool> cancelBooking(b.Booking data) async {
      //send data to API
      final response = await client.post(
        "$apiUrl/api/cancelbooking",
        headers: {"content-type": "application/json"},
        body: b.cancelBookingToJson(data),
      );
      if (response.statusCode == 200) {
        return Future<bool>.value(true);
      } else {
        return Future<bool>.value(false);
    }
  }

  Future<bool> readNotification(String bookingNo ) async {
      //send data to API
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String username = prefs.getString("username");

      final response = await client.post(
        "$_apiURL/api/read_notif",
        headers: {"content-type": "application/json"},
        body: notifToJson01(bookingNo,username),
      );
      if (response.statusCode == 200) {
        return Future<bool>.value(true);
      } else {
        return Future<bool>.value(false);
    }
  }

  Future<LoginStatus> login(String username,String password) async{
    final response = await client.post("$_apiURL/api/login/"+username + "/"+password);
    if (response.statusCode == 200) {
       return loginStatusFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<String> logout(String username) async{
    final response = await client.post("$_apiURL/api/logout/"+username );
    if (response.statusCode == 200) {
       return logoutFromJson(response.body);
    } else {
      return null;
    }
  }

  
  Future<bool> releaseBooking(b.Booking data) async {
      //send data to API
      final response = await client.post(
        "$apiUrl/api/release_booking",
        headers: {"content-type": "application/json"},
        body: b.releaaseBookingToJson(data),
      );
      if (response.statusCode == 200) {
        return Future<bool>.value(true);
      } else {
        return Future<bool>.value(false);
    }
  }

  Future<bool> approvalBooking(b.Booking data,int flag) async {
      //send data to API
      if(flag==0)//reject
        data.bookingStatus=3;
      else // approved
        data.bookingStatus=2;
      final response = await client.post(
        "$apiUrl/api/approvalbooking",
        headers: {"content-type": "application/json"},
        body: b.approvalBookingToJson(data),
      );
      if (response.statusCode == 200) {
        return Future<bool>.value(true);
      } else {
        return Future<bool>.value(false);
    }
  }

  Future<BookingStatus> createBooking(a.Booking data) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String username = prefs.getString("username");
      data.username=username;
      //send data to API
      final response = await client.post(
        "$apiUrl/api/booking",
        headers: {"content-type": "application/json"},
        body: a.bookingToJson(data),
      );
      if (response.statusCode == 200) {
          return bookingStatusFromJson(response.body);
        //return true;
      } else {
        return null;
    }
  }
}

