
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:pkool/contactpage.dart';
import 'package:pkool/home_admin_page.dart';
import 'package:pkool/home_glum_page.dart';
import 'package:pkool/home_page.dart';
import 'package:pkool/models/login_status.dart';
import 'package:pkool/service_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home_page.dart';
import 'contactpage.dart';
import 'package:url_launcher/url_launcher.dart';


class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}


class _LoginPageState extends State<LoginPage> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey(debugLabel: "Main Navigator");
    static const _lpscolor = Color.fromRGBO(226,135, 38, 1);
  SharedPreferences sharedPreferences;
  bool checkValue = false;
  String statusLogin='';
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();

  @override
  void initState() {
    super.initState();
    getCredential();
    GlobalConfiguration().loadFromAsset("app_settings");
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      checkValue = sharedPreferences.getBool("check");
      if (checkValue != null) {
        if (checkValue) {
          username.text = sharedPreferences.getString("username");
          password.text = sharedPreferences.getString("password");
        } else {
          username.clear();
          password.clear();
          sharedPreferences.clear();
        }
      } else {
        checkValue = false;
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/lps_logo.png'),
      ),
    );

    final email = TextField(
      autofocus: false,
      controller: username,
      //initialValue: 'put your username here...',
      decoration: InputDecoration(
        hintText: 'Username',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final passwd = TextField(
      autofocus: false,
      //initialValue: 'put your username here...',
      controller: password,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    _onChanged(bool value) async {
      sharedPreferences = await SharedPreferences.getInstance();
      setState(() async {
        checkValue = value;
        await sharedPreferences.setBool("check", checkValue);
        await sharedPreferences.setString("username", username.text);
        await sharedPreferences.setString("password", password.text);
        getCredential();
      });
    }

  _launchURL() async {
    const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _showDialogAbout() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Tentang Aplikasi Pkool"),
          content: new RichText(
              text: new TextSpan(
              children: [
                new TextSpan(
                  text: 'Silahkan baca, ',
                  style: new TextStyle(color: Colors.black),
                ),
                new TextSpan(
                  text: 'Petunjuk Aplikasi Mobil Operasional LPS',
                  style: new TextStyle(color: Colors.blue),
                  recognizer: new TapGestureRecognizer()..onTap = () { _launchURL();
                  },
                ),
                new TextSpan(
                  text: ' Sehingga mengerti bagaimana menggunakannya.',
                  style: new TextStyle(color: Colors.black),
                ),
              ],
              ),
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              color: Colors.orangeAccent,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

    _showDialog(String msg)
    {
        showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              msg
              ,
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }

    void _login(username,password) async {
        ApiService _apiService = ApiService();
        _apiService.login(username,password).then((loginStatus) {
            LoginStatus _loginStatus = loginStatus;
            if(_loginStatus.status!="OK" || _loginStatus.status.length==0)
            {
              _showDialog(loginStatus.status);
              setState(() {
                statusLogin="Login Fail";
              });
            }
            else
            {
              statusLogin=_loginStatus.status;
              int roleid = _loginStatus.roleid;
              sharedPreferences.setString('username', username);
              sharedPreferences.setInt('roleid', roleid);
              if(roleid==4)
              {
                Navigator.of(context).pushNamed(HomeAdminPage.tag);
                setState(() {
                    username= this.username;
                });
              }
              else if(roleid==3)
              {
                Navigator.of(context).pushNamed(HomeGLUMPage.tag);
                setState(() {
                    username= this.username;
                });
              }
              else
              {
                Navigator.of(context).pushNamed(HomePage.tag);
                setState(() {
                    username= this.username;
                });
              }
            }
        });
    }

    _navigator() {
    if (username.text.trim().length != 0 && password.text.trim().length != 0) {
      _login(username.text.trim(),password.text.trim());
    } else {
      _showDialog("username or password \ncan't be empty");
    }
  }

  final rememberMe = CheckboxListTile(
      value: checkValue,
      onChanged: _onChanged,
      title: new Text("Remember me"),
      controlAffinity: ListTileControlAffinity.leading,
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.orangeAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            _navigator();
            //Navigator.of(context).pushNamed(HomePage.tag);
          },
          color: _lpscolor,
          child: Text('Log In', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Contact Helpdesk ?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
        Navigator.of(context).pushAndRemoveUntil(
              new MaterialPageRoute(
                  builder: (BuildContext context) => new ContactPage()),
              (Route<dynamic> route) => false); 
        //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ContactsPage()));
      },
    );

    final needHelpLabel = FlatButton(
      child: Text(
        'Need Help ?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
        _showDialogAbout();
        //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ContactsPage()));
      },
    );
    
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            email,
            SizedBox(height: 8.0),
            passwd,
            SizedBox(height: 24.0),
            rememberMe,
            loginButton,
            forgotLabel,
            needHelpLabel
          ],
        ),
      ),
    );
  }
}


      



