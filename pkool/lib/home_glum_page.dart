import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:pkool/login_page.dart';
import 'package:pkool/profile.dart';
import 'package:pkool/service_api.dart';
import 'constants.dart';
import 'models/dashboard.dart';
import 'models/chartData.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
class HomeGLUMPage extends StatefulWidget
{
  static String tag = 'home-glum-page';
  
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<HomeGLUMPage>
{
  ScrollController _scrollController = new ScrollController();
  List<DashboardData> listDashboard;
  List<ChartData> listChart;
  var isLoading = false;
  ApiService _api = ApiService();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
    new GlobalKey<RefreshIndicatorState>();

  List<List<double>> charts=[];
  
  _contructData(List<ChartData> lst)
  {
    List<double> data1=[]; //data mingguan
    List<double> data2=[]; //data bulanan
    List<double> data3=[]; //data tahunan
    var now = DateTime.now();
    var lastWeek = now.add(new Duration(days: -7));
    var lastMonth = now.add(new Duration(days:-30));
    var lastYear = now.add(new Duration(days: -360));

    String _lastweek = DateFormat('yyyyMMdd').format(lastWeek);
    String _lastMonth = DateFormat('yyyyMMdd').format(lastMonth);
    String _lastYear = DateFormat('yyyyMMdd').format(lastYear);

    for (var name in lst) {
      if(_lastweek.compareTo(name.date) <=0)
        data1.add(name.value.toDouble());
      if(_lastMonth.compareTo(name.date) <= 0)
        data2.add(name.value.toDouble());
      if(_lastYear.compareTo(name.date) <= 0)
        data3.add(name.value.toDouble());
    }
    charts.add(data1);
    charts.add(data2);
    charts.add(data3);
  }

  _fetchData() async 
  {
    setState(() {
      isLoading = true;
    });
    listDashboard = await _api.getDashboardData();
    listChart = await _api.getChartData();
    _contructData(listChart);
    setState(() {
        isLoading = false;
    });
  }

  @override
  void initState() 
  {
    _fetchData();
    super.initState();
    WidgetsBinding.instance
      .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
    _scrollController.addListener(() {
        
    });
  }

  static final List<String> chartDropdownItems = [ 'Last 7 days', 'Last month', 'Last year' ];
  String actualDropdown = chartDropdownItems[0];
  int actualChart = 0;

  void choiceAction(Choice choice) async {
        if(choice.title == Constants.SignOut){
            _logout();
        }
        else if(choice.title==Constants.Profile)
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ProfilePage()));
        }
    }


  @override
  Widget build(BuildContext context)
  {
    return Scaffold
    (
      appBar: AppBar
      (
        automaticallyImplyLeading: false,
        backgroundColor: Colors.orangeAccent,
        title: Text('Dashboard', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700)),
        /*
        leading: new IconButton(
          icon: new Icon(Icons.home, color: Colors.black),
          onPressed: () => _logout(),
        ),
        */
        actions: <Widget>[
              PopupMenuButton<Choice>(
                  onSelected: choiceAction,
                  icon: Icon(Icons.person),
                  itemBuilder: (BuildContext context){
                      return Constants.choices.map((Choice choice){
                          return PopupMenuItem<Choice>(
                              value: choice,
                              child: Row(
                                children: <Widget>[
                                    Icon(choice.icon),
                                    Text(choice.title),
                                ],
                              ),);
                              //child: Text(choice.title),);
                      })
                          .toList();
                  }
                  ,)]
                  ,
      ),
      body:  isLoading
            ? Center(
                child: CircularProgressIndicator(),
            ) 
        :StaggeredGridView.count(
        controller: _scrollController,
        crossAxisCount: 2,
        crossAxisSpacing: 12.0,
        mainAxisSpacing: 12.0,
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        children: <Widget>[
          _buildTile(
            Padding
            (
              padding: const EdgeInsets.all(24.0),
              child: Row
              (
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>
                [
                  Column
                  (
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>
                    [
                      Text('Total Booking Today', style: TextStyle(color: Colors.blueAccent)),
                      Text(listDashboard[0].totalBooking.toString(), style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0))
                    ],
                  ),
                  Material
                  (
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(24.0),
                    child: Center
                    (
                      child: Padding
                      (
                        padding: const EdgeInsets.all(16.0),
                        child: Icon(Icons.timeline, color: Colors.white, size: 30.0),
                      )
                    )
                  )
                ]
              ),
            ),
          ),
          _buildTile(
            Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.teal,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: const EdgeInsets.all(16.0),
                      child: Icon(Icons.directions_car, color: Colors.white, size: 30.0),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text('Approved', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 18.0)),
                  Text(listDashboard[0].totalApproved.toString(), style: TextStyle(color:  Colors.black45, fontWeight: FontWeight.w700, fontSize: 18.0)),
                ]
              ),
            ),
          ),
          _buildTile(
            Padding
            (
              padding: const EdgeInsets.all(24.0),
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>
                [
                  Material
                  (
                    color: Colors.amber,
                    shape: CircleBorder(),
                    child: Padding
                    (
                      padding: EdgeInsets.all(16.0),
                      child: Icon(Icons.cancel, color: Colors.white, size: 30.0),
                    )
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text('Dibatalkan', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 18.0)),
                  Text(listDashboard[0].totalCancelled.toString(), style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w700, fontSize: 18.0)),
                ]
              ),
            ),
          ),
          _buildTile(
            Padding
                (
                  padding: const EdgeInsets.all(24.0),
                  child: Column
                  (
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>
                    [
                      Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              Text('Trend Pemakaian', style: TextStyle(color: Colors.blue)),
                            ],
                          ),
                          DropdownButton
                          (
                            isDense: true,
                            value: actualDropdown,
                            underline: Container(
                              height: 2,
                              color: Colors.orangeAccent,
                            ),
                            icon: Icon(Icons.arrow_downward),
                            iconSize: 20.0, 
                            iconEnabledColor: Colors.blue,
                            items: chartDropdownItems.map((String title)
                            {
                              return DropdownMenuItem
                              (
                                value: title,
                                child: Text(title, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w400, fontSize: 14.0)),
                              );
                            }).toList(),
                            onChanged: (String value) => setState(()
                            {
                              actualDropdown = value;
                              actualChart = chartDropdownItems.indexOf(value); // Refresh the chart
                            }),
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 50.0)),
                      Sparkline
                      (
                        data: charts[actualChart],
                        lineWidth: 4.0,
                        lineColor: Colors.deepOrangeAccent,
                        pointsMode: PointsMode.all,
                        pointSize: 8.0,
                        pointColor: Colors.amber,
                      )
                    ],
                  )
                ),
          ),
        ],
        staggeredTiles: [
          StaggeredTile.extent(2, 110.0),
          StaggeredTile.extent(1, 180.0),
          StaggeredTile.extent(1, 180.0),
          StaggeredTile.extent(2, 260.0),
        ],
      )
    );
  }
  Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
      elevation: 14.0,
      borderRadius: BorderRadius.circular(12.0),
      shadowColor: Color(0x802196F3),
      child: InkWell
      (
        // Do onTap() if it isn't null, otherwise do print()
        onTap: onTap != null ? () => onTap() : () {  },
        child: child
      )
    );
  }

  _showDialog(String msg)
    {
        showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              msg
              ,
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }

  _logout() async
    {
        //get session
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String username = prefs.getString("username");

        //send activity to database
        ApiService _apiService = ApiService();
        _apiService.logout(username).then((status) {
            String _status=status;
            if(_status=="OK")
            {
              //remove session
              bool checkValue = prefs.getBool("check");
              if(null!=checkValue && !checkValue)
                prefs.remove('username');
              // go back to login page
              Navigator.of(context).pushAndRemoveUntil(
              new MaterialPageRoute(
                  builder: (BuildContext context) => new LoginPage()),
              (Route<dynamic> route) => false);
            }
            else{
                _showDialog("Failed to logout");
            }
        });
    }
}

