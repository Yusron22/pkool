import 'dart:convert';

class Profile {
  String name;
  String email;
  String mobileno;

  Profile({ this.name, this.email, this.mobileno});

  factory Profile.fromJson(Map<String, dynamic> map) {
    return Profile(
        name: map["fullname"], email: map["email"], mobileno: map["mobileno"]);
  }

  Map<String, dynamic> toJson() {
    return {"fullname": name, "email": email, "mobileno": mobileno};
  }

  @override
  String toString() {
    return 'Profile{ fullname: $name, email: $email, mobileno: $mobileno}';
  }

}

List<Profile> profileFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Profile>.from(data.map((item) => Profile.fromJson(item)));
}

String profileToJson(Profile data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}