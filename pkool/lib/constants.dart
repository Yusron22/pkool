import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Constants{
  static const SignOut = "Sign out";
  static const Profile = "Profile";
  static const List<Choice> choices = const <Choice>[
    const Choice(title: Profile, icon: Icons.person),
    const Choice(title: SignOut, icon: Icons.exit_to_app),
  ];
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}