import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:pkool/home_page.dart';
import 'package:pkool/models/booking.dart';
import '../models/booking.dart';
import 'package:pkool/service_api.dart';
import 'package:shared_preferences/shared_preferences.dart';


class BookingInboxPage extends StatefulWidget {
  @override
  _BookingInboxPageState createState() => _BookingInboxPageState();
}


class _BookingInboxPageState extends State<BookingInboxPage> {
  SharedPreferences sharedPreferences;
  ApiService _apiService = ApiService();
  List<Booking> list = List();
  var isLoading = false;
  int roleId =0; 

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    roleId=sharedPreferences.getInt("roleid");
  }


  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
    new GlobalKey<RefreshIndicatorState>();

  @override
  void initState() 
  {
    getCredential();
    _fetchData();
    super.initState();
    WidgetsBinding.instance
      .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
  }

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    list =await _apiService.getBooking();
    setState(() {
        isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.home, color: Colors.black),
            onPressed: () => Navigator.of(context).pushNamed(HomePage.tag),
          ),
          backgroundColor: Colors.orangeAccent,
          title: Text("Booking List"),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.separated(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: Icon(Icons.directions_car,size:42),
                    title: new Text(list[index].notes + '\n' + list[index].bookingBy + '\n'+ list[index].bookingNo.toString()+ ' - ' + list[index].bookingTo ),
                    subtitle: Text(list[index].bookingTimeFrom.toString()+'\n'+ list[index].bookingTimeTo.toString()+'\n'+list[index].bookingReason ) ,
                    trailing:(list[index].bookingStatus==1 ? (roleId==1?
                          new IconButton(
                            icon: new Icon(Icons.delete),
                            onPressed: () { 
                              /* Call API Service To Cancel Booking */ 
                              Future result = _apiService.cancelBooking(list[index]);
                              result.then((value) => _fetchData());
                            },
                          )
                          :(roleId==2?
                          Wrap(         
                            spacing:12, 
                            children: <Widget>[
                              new IconButton(
                                icon: new Icon(Icons.delete),
                                onPressed: () { 
                                  /* Your code */ 
                                  Future result = _apiService.approvalBooking(list[index],0);
                                  result.then((value) => _fetchData());
                                },
                              ),
                              new IconButton(
                                icon: new Icon(Icons.check_circle_outline),
                                onPressed: () { 
                                  /* Your code */ 
                                    Future result = _apiService.approvalBooking(list[index],1);
                                    result.then((value) => _fetchData());
                                  },
                              ),
                          ])
                          :null)):null)
                  );
                },
                separatorBuilder: (BuildContext context,int index){
                    return Divider();
                }),
                floatingActionButton: FloatingActionButton(
                    //child: Text('Booking'),
                    child: const Icon(Icons.refresh),
                    backgroundColor: Colors.deepOrange.shade400,
                    onPressed: (){
                        //go to booking page
                        _fetchData();
                    },
                  ),
                );
  }
}

