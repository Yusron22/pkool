import 'package:flutter/material.dart';
import 'package:pkool/booking_page.dart';
import 'package:pkool/home_page.dart';
import 'package:pkool/home_admin_page.dart';
import '../scrolling-day-calendar.dart';
import 'package:intl/intl.dart';
import 'package:pkool/service_api.dart';
import '../models/booking_daily_calendar.dart';
import '../home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SearchBookingPage extends StatefulWidget {
  @override
  _PageTwoState createState() => _PageTwoState();
}

class _PageTwoState extends State<SearchBookingPage> {
  ApiService _apiService = ApiService();
  List<Booking> list = List();
  var isLoading = false;
  DateTime selectedDate = DateTime.now();
  DateTime startDate = DateTime.now().subtract(Duration(days: 10));
  DateTime endDate = DateTime.now().add(Duration(days: 10));
  String widgetKeyFormat = "yyyy-MM-dd";
  SharedPreferences sharedPreferences;
  int roleId;
  var testData;

  init() async
  {
      sharedPreferences = await SharedPreferences.getInstance();
      roleId=sharedPreferences.getInt("roleid");
  }

  @override
  initState()
  {
    init();
    _fetchData();
    super.initState();
  }

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    list =await _apiService.getBookingCalendar();
    testData = <String, List<Booking>>{};
    for (var item in list) {
      testData.putIfAbsent(item.bookingDate, () => <Booking>[]).add(item);
    }
    setState(() {
        isLoading = false;
        pageItems = _widgetBuilder(selectedDate);
    });
  }

  Widget pageItems = Center(
    child: Text("No events"),
  );

  _widgetBuilder(DateTime selectedDate) {
    String dateString = DateFormat(widgetKeyFormat).format(selectedDate);

    // find the child records in test data and build widgets
    if (testData.containsKey(dateString)) {
      List items = testData[dateString];

      return ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, key) {
          return Card(
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 120.0,
                      height: 25.0,
                      
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        border: Border.all(width: 1.0, color: Colors.orangeAccent),
                      ),
                      
                      child: Center(
                        child: Text(
                          items[key].bookingBy,
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          items[key].timeFrom,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Icon(
                            Icons.directions_car,
                            //color: Colors.white ,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            items[key].timeTo,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    items[key].bookingTo + ' ['+ items[key].notes+ ']',
                    style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                  ),
                  Text(items[key].bookingReason)
                ],
              ),
            ),
          );
        },
      );
    }

    // default widget to display on the page
    return Center(
      child: Text("No events"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.home, color: Colors.black),
            onPressed: () => Navigator.of(context).pushNamed((roleId==4?HomeAdminPage.tag:HomePage.tag)),
          ),
          backgroundColor: Colors.deepOrangeAccent,
          centerTitle: true,
          title: Text("Booking Calendar"),
        ),
        body: ScrollingDayCalendar(
          startDate: startDate,
          endDate: endDate,
          selectedDate: selectedDate,
          onDateChange: (direction, DateTime selectedDate) {
            setState(() {
              pageItems = _widgetBuilder(selectedDate);
            });
          },
          dateStyle: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
          pageItems: pageItems,
          displayDateFormat: "dd/MM/yyyy",
          dateBackgroundColor: Colors.grey,
          forwardIcon: Icons.arrow_forward,
          backwardIcon: Icons.arrow_back,
          pageChangeDuration: Duration(
            milliseconds: 400,
          ),
          noItemsWidget: Center(
            child: Text("No items have been added for this date"),
          ),
        ),
        floatingActionButton: new Visibility(
          visible: (roleId==1? true: false),
          child: FloatingActionButton(
          //child: Text('Booking'),
          child: const Icon(Icons.directions_car),
          backgroundColor: Colors.deepOrange.shade400,
          onPressed: (){
            //go to booking page
            Route route = MaterialPageRoute(builder: (context) => BookingPage());
            Navigator.push(context, route);
            //Navigator.pop(context);
          },
          ),
        ),
      ),
    );
  }
}