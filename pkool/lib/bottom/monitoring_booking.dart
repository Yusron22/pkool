import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:pkool/bottom/time_line.dart';
import 'package:pkool/home_page.dart';
import 'package:pkool/home_admin_page.dart';
import 'package:pkool/models/booking.dart';
import '../models/booking.dart';
import 'package:pkool/service_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'time_line.dart';
class MonitoringBookingPage extends StatefulWidget {

  @override
  _MonitoringBookingPageState createState() => _MonitoringBookingPageState();
}


class _MonitoringBookingPageState extends State<MonitoringBookingPage> {
  SharedPreferences sharedPreferences;
  ApiService _apiService = ApiService();
  List<Booking> list = List();
  var isLoading = false;
  int roleId; 

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    roleId=sharedPreferences.getInt("roleid");
  }
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
    new GlobalKey<RefreshIndicatorState>();

  @override
  void initState() 
  {
    getCredential();
    _fetchData();
    super.initState();
    WidgetsBinding.instance
      .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
  }

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    list =await _apiService.getBooking();
    setState(() {
        isLoading = false;
    });
  }

  Widget pageItems = Center(
    child: Text("No Items"),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.home, color: Colors.black),
            onPressed: () => Navigator.of(context).pushNamed((roleId==4?HomeAdminPage.tag:HomePage.tag)),
          ),
          backgroundColor: Colors.orangeAccent,
          title: Text("Monitoring Booking List"),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : 
            list==null ? pageItems
            :
            ListView.separated(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: Icon(Icons.directions_car,size:42),
                    title: new Text(list[index].notes + '\n' + list[index].bookingBy + '\n'+ list[index].bookingNo.toString()+ ' - ' + list[index].bookingTo ),
                    subtitle: Text(list[index].bookingTimeFrom.toString()+'\n'+ list[index].bookingTimeTo.toString()+'\n'+list[index].bookingReason ) ,
                    trailing:new IconButton(
                            icon: new Icon((roleId==4? Icons.remove_from_queue :Icons.timeline)),
                            onPressed: () { 
                              /* Call API Service To Cancel Booking */ 
                              if(roleId==4)
                              {
                                Future result = _apiService.releaseBooking(list[index]);
                                result.then((value) => _fetchData());
                              }
                              else
                              {
                                Navigator.push(context,
                                  MaterialPageRoute(builder: (context) => TimelinePage(bookingNo : list[index].bookingNo)));
                              }
                            },
                          ),
                  );
                },
                separatorBuilder: (BuildContext context,int index){
                    return Divider();
                }),
                floatingActionButton: FloatingActionButton(
                    //child: Text('Booking'),
                    child: const Icon(Icons.refresh),
                    backgroundColor: Colors.deepOrange.shade400,
                    onPressed: (){
                        //go to booking page
                        _fetchData();
                    },
                  ),
                );
  }
}

