import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pkool/booking_page.dart';
import 'package:pkool/login_page.dart';
import 'package:pkool/profile.dart';
import 'constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'bottom/inbox_booking.dart';
import 'bottom/search_booking.dart';
import 'bottom/monitoring_booking.dart';
import 'package:pkool/service_api.dart';

class HomePage extends StatelessWidget {
  static String tag = 'home-page';
  static const IconData car = IconData(0xf36f, fontFamily:CupertinoIcons.iconFont, fontPackage: CupertinoIcons.iconFontPackage);
  @override
  Widget build(BuildContext context) {
    final alucard = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/Rubicon.png'),
        ),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Selamat Datang di Aplikasi Mobil Operasional LPS',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final lorem = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Pegawai LPS dapat melakukan pemesanan Mobil Operasional Dengan menggunakan Aplikasi ini, silahkan Tekan menu Booking untuk melakukan pemesanan.',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.orangeAccent,
          Colors.orangeAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[alucard, welcome, lorem],
      ),
    );

    _showDialog(String msg)
    {
        showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              msg
              ,
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text("OK"))
            ],
          ));
    }

    _logout() async
    {
        //get session
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String username = prefs.getString("username");

        //send activity to database
        ApiService _apiService = ApiService();
        _apiService.logout(username).then((status) {
            String _status=status;
            if(_status=="OK")
            {
              //remove session
              bool checkValue = prefs.getBool("check");
              if(null!=checkValue && !checkValue)
                prefs.remove('username');
              // go back to login page
              Navigator.of(context).pushAndRemoveUntil(
              new MaterialPageRoute(
                  builder: (BuildContext context) => new LoginPage()),
              (Route<dynamic> route) => false);
            }
            else{
                _showDialog("Failed to logout");
            }
        });
    }

    void choiceAction(Choice choice) async {
        if(choice.title == Constants.SignOut){
            _logout();
        }
        else if(choice.title==Constants.Profile)
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ProfilePage()));
        }
    }

    int _cIndex = 0;
    void _incrementTab(index) {
        _cIndex = index;
        if(_cIndex==0) //My Booking
        {
            Navigator.push(context,
              MaterialPageRoute(builder: (context) => BookingInboxPage()));
        }
        else if(_cIndex==1)
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => SearchBookingPage()));
        }
        else if(_cIndex==2)
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => MonitoringBookingPage()));
        }
    }

    return Scaffold(
      appBar: new AppBar(
          elevation: 0.0,
          title: const Text(''),
          backgroundColor: Colors.orangeAccent,
          automaticallyImplyLeading: false,
          actions: <Widget>[
              PopupMenuButton<Choice>(
                  onSelected: choiceAction,
                  icon: Icon(Icons.person),
                  itemBuilder: (BuildContext context){
                      return Constants.choices.map((Choice choice){
                          return PopupMenuItem<Choice>(
                              value: choice,
                              child: Row(
                                children: <Widget>[
                                    Icon(choice.icon),
                                    Text(choice.title),
                                ],
                              ),);
                              //child: Text(choice.title),);
                      })
                          .toList();
                  }
                  ,)]
                  ,
          ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor:Colors.orange,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black,
        type: BottomNavigationBarType.fixed,
        //currentIndex: 1,
        onTap: (int index) {
            _incrementTab(index);
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.inbox), title: Text('My Booking')),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today), title: Text('Calendar')),
          BottomNavigationBarItem(
              icon: Icon(Icons.mobile_screen_share), title: Text('Monitoring')),
        ],
      ),
      body: body,
      floatingActionButton: FloatingActionButton(
        //child: Text('Booking'),
        child: const Icon(car),
        backgroundColor: Colors.deepOrange.shade400,
        onPressed: (){
            //go to booking page
            Navigator.push(context,
            MaterialPageRoute(builder: (context) => BookingPage()));
        },
      ),
    );
  }
}
