import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:pkool/models/booking_status.dart';
import 'service_api.dart';
import 'models/booking_model.dart';
import 'bottom/inbox_booking.dart';
import 'home_page.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class BookingPage extends StatefulWidget {
  static const String routeName = "/BookingPage";
  @override
  _BookingPageState createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {
  bool _isLoading = false;
  ApiService _apiService = ApiService();
  bool _isFieldTimeFromValid;
  bool _isFieldTimeToValid;
  bool _isFieldToValid;
  bool _isFieldReasonValid;
  DateTime dateFrom;
  DateTime dateTo;
  TextEditingController _controllerTo = TextEditingController();
  TextEditingController _controllerReason = TextEditingController();
  _showDialog(String msg,int status)
    {
        showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              msg
              ,
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    if(status==1)
                    {
                      
                      //Navigator.pop(_scaffoldState.currentState.context);
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => BookingInboxPage()),
                      );
                    }
                  },
                  child: new Text("OK"))
            ],
          ));
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.home, color: Colors.black),
          onPressed: () => Navigator.of(context).pushNamed(HomePage.tag),
        ),
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.orangeAccent,
        title: Text(
          "Form Pemesanan",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView ( // this will make your body scrollable
        child: Column(
        /// your parameters
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _buildTimeFrom(),
                _buildTimeTo(),
                _buildTextFieldTo(),
                _buildTextFieldReason(),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: RaisedButton(
                    onPressed: () {
                      if (_isFieldToValid == null ||
                          _isFieldReasonValid == null ||
                          _isFieldTimeFromValid==null ||
                          _isFieldTimeToValid==null ||
                          !_isFieldTimeFromValid ||
                          !_isFieldTimeToValid ||
                          !_isFieldToValid ||
                           !_isFieldReasonValid){
                        _scaffoldState.currentState.showSnackBar(
                          SnackBar(
                            content: Text("Form Pemesanan harus diisi seluruhnya."),
                          ),
                        );
                        return;
                      }
                      if(dateTo.isBefore(dateFrom))
                      {
                          _scaffoldState.currentState.showSnackBar(
                          SnackBar(
                            content: Text("Waktu Mulai harus lebih kecil dibanding Waktu Selesai!"),
                          ),
                        );
                        return;
                      }

                      var now = DateTime.now();
                      DateTime dt = new DateTime(now.year, now.month, now.day, dateFrom.hour,dateFrom.minute);
                      String _dateFrom = DateFormat('yyyy-MM-dd kk:mm').format(dt);
                      final difference = dt.difference(now).inMinutes;
                      if(difference<=60)
                      {
                          _scaffoldState.currentState.showSnackBar(
                          SnackBar(
                            content: Text("Pemesanan mobil dapat dilakukan minimal 1 jam sebelum keberangkatan dan sudah mendapat persetujuan kepala divisi"),
                          ),
                        );
                        return;
                      }


                      dt = new DateTime(now.year, now.month, now.day, dateTo.hour,dateTo.minute);
                      String _dateTo = DateFormat('yyyy-MM-dd kk:mm').format(dt);
                      String tujuan = _controllerTo.text.toString();
                      String alasan = _controllerReason.text.toString();
                      String username ='';
                      setState (() =>  _isLoading = true);

                      Booking booking =
                          Booking(username: username,datefrom: _dateFrom,dateto: _dateTo,tujuan: tujuan,alasan: alasan);
                      _apiService.createBooking(booking).then((bookingStatus) {
                        setState(() => _isLoading = false);
                        BookingStatus status = bookingStatus;
                        if (status.bookingNo=="0")  {
                          _showDialog("Maaf, anda tidak dapat melakukan pemesanan pada jam tersebut. karena sudah ada yang melakukan pemesanan terlebih dahulu pada jam tersebut.",0);
                        }
                        else if(status.bookingNo!="0" && status.notes.toUpperCase()=="OK")
                        {
                          _showDialog("Pesanan kendaraan anda berhasil dikirim, Menunggu proses approval.",1);
                        }
                         else {
                          _scaffoldState.currentState.showSnackBar(SnackBar(
                            content: Text("Submit data failed"),
                          ));
                        }
                      });
                    },
                    child: Text(
                      "Submit".toUpperCase(),
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    color: Colors.orange[600],
                  ),
                )
              ],
            ),
          ),
          _isLoading
              ? Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: 0.3,
                      child: ModalBarrier(
                        dismissible: false,
                        color: Colors.grey,
                      ),
                    ),
                    Center(
                      child: CircularProgressIndicator(),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
      )
    );
  }
  /*
  Widget _buildTextFieldTimeFrom() {
    return TextField(
      controller: _controllerName,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Waktu Mulai",
        errorText: _isFieldTimeFromValid == null || _isFieldTimeFromValid
            ? null
            : "Waktu Mulai diperlukan",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldTimeFromValid) {
          setState(() => _isFieldTimeFromValid= isFieldValid);
        }
      },
    );
  }
*/
/*
  Widget _buildTextFieldTimeTo() {
    return TextField(
      controller: _controllerEmail,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: "Waktu Selesai",
        errorText: _isFieldTimeToValid == null || _isFieldTimeToValid
            ? null
            : "Sampai Selesai diperlukan",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldTimeToValid) {
          setState(() => _isFieldTimeToValid = isFieldValid);
        }
      },
    );
  }
  */
  Widget _buildTextFieldTo() {
    return TextField(
      maxLines: 3,
      controller: _controllerTo,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Tujuan",
        errorText: _isFieldToValid == null || _isFieldToValid
            ? null
            : "Tujuan diperlukan",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldToValid) {
          setState(() => _isFieldToValid = isFieldValid);
        }
      },
    );
  }
  Widget _buildTextFieldReason() {
    return TextField(
      maxLines: 3,
      controller: _controllerReason,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: "Alasan Penggunaan",
        errorText: _isFieldReasonValid == null || _isFieldReasonValid
            ? null
            : "Alasan Penggunaan Diperlukan",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldReasonValid) {
          setState(() => _isFieldReasonValid = isFieldValid);
        }
      },
    );
  }
  final format = DateFormat("HH:mm");
  Widget _buildTimeFrom() {
    return Column(children: <Widget>[
      //Text('Waktu Mulai',textAlign: TextAlign.left),
      DateTimeField(
        format: format,
        decoration: InputDecoration(
        labelText: "Waktu Mulai",
        errorText: _isFieldTimeFromValid == null || _isFieldTimeFromValid
            ? null
            : "Waktu Mulai Diperlukan",
        ),
        onChanged: (DateTime newValue) {
          setState(() {
            dateFrom = newValue;
            _isFieldTimeFromValid= true;
          });
        },
        onShowPicker: (context, currentValue) async {
          final time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
          );
          return DateTimeField.convert(time);
        },
      ),
    ]);
  }
  Widget _buildTimeTo() {
    return Column(children: <Widget>[
      //Text('Waktu Selesai',textAlign: TextAlign.left),
      DateTimeField(
        format: format,
        decoration: InputDecoration(
        labelText: "Waktu Selesai",
        errorText: _isFieldTimeToValid == null || _isFieldTimeToValid
            ? null
            : "Waktu Selesai Diperlukan",
        ),
        onChanged: (DateTime newValue) {
          setState(() {
            dateTo = newValue;
            _isFieldTimeToValid= true;
          });
        },
        onShowPicker: (context, currentValue) async {
          final time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
          );
          return DateTimeField.convert(time);
        },
      ),
    ]);
  }  
}