import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:pkool/service_api.dart';
import 'models/contact.dart';
import 'login_page.dart';

class ContactPage extends StatefulWidget {
  @override
  _ContactPageState createState() => _ContactPageState();
}


class _ContactPageState extends State<ContactPage> {
  ApiService _apiService = ApiService();
  List<Contact> list = List();
  var isLoading = false;
  int roleId =0; 

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
    new GlobalKey<RefreshIndicatorState>();

  @override
  void initState() 
  {
    _fetchData();
    super.initState();
    WidgetsBinding.instance
      .addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
  }

  _fetchData() async {
    setState(() {
      isLoading = true;
    });
    list =await _apiService.getContactHelpdesk();
    setState(() {
        isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pushAndRemoveUntil(
          new MaterialPageRoute(
              builder: (BuildContext context) => new LoginPage()),
          (Route<dynamic> route) => false),
          ),
          backgroundColor: Colors.orangeAccent,
          title: Text("Contact Helpdesk"),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.separated(
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    contentPadding: EdgeInsets.all(10.0),
                    leading: Icon(Icons.email,size:42),
                    title: new Text(list[index].fullname),
                    subtitle: new Text(list[index].email)
                  );
                },
                separatorBuilder: (BuildContext context,int index){
                    return Divider();
                }),
                );
  }
}

